import {HOST_API} from "@/constants/constants";

const programs = '9e92a3cb-5922-4d2d-aab6-346b7672f3ae'
const headers = { "Content-Type": "application/json" }

// info: в реальном приложении поверку ответа надо вынести в отдельную функцию, которую можно будет переиспользовать
export const get = async () => {
  return fetch(HOST_API + programs, {headers})
    .then(async response => {
      const data = await response.json()
      if (!response.ok) {
        const error = (data && data.message) || response.statusText
        return Promise.reject(error)
      }
      return data
    })
    .catch(error => {
      console.error("Ошибка при запросе данных!", error);
    })
}

export default { get }