let cartItems = window.localStorage.getItem('cartItems')
export default {
  state: {
    programs: cartItems ? JSON.parse(cartItems) : []
  },
  getters: {
    amount: state => state.programs.length,
    total(state) {
      // // console.log(state.programs[0].price)
      return state.programs.reduce((accum, curr) => {
        return accum + curr.price
      }, 0)
    },
    has(state) {
      return function (id) {
        return state.programs.some(value => value.id === id)
      }
    },
    programs: state => state.programs
  },
  mutations: {
    add(state, params) {
      state.programs.push({id: params.id, price: params.price, title: params.title})
      // console.log(state.programs)
			this.commit('saveCart')
    },
    remove(state, id) {
      state.programs = state.programs.filter(pr => pr.id !== id)
			this.commit('saveCart')
    },
    clear(state) {
      state.programs = []
			this.commit('saveCart')
    },
    saveCart(state) {
      window.localStorage.setItem('cartItems', JSON.stringify(state.programs))
    }
  },
  actions: {
    add(store, params) {
      if (!store.getters.has(params.id)) {
        store.commit('add', {id: params.id, price: params.price, title: params.title})
      }
    },
    remove(store, id) {
      if (store.getters.has(id)) {
        store.commit('remove', id)
      }
    },
    clear(store) {
      store.commit('clear')
    }
  }
}