import Vue from 'vue'
import Vuex from 'vuex'
import cart from './cart';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    programsList: [],
    selectedProgramCategory: 0,
    searchString: '',
    activeCardProgramSlug: '',
    countElementOnPage: 0
  },
  mutations: {
    selectProgramCategory(state, val) {
      state.selectedProgramCategory = val
    },
    setProgramsList(state, programs) {
      state.programsList = programs
    },
    setSearchString(state, val) {
      state.searchString = val
    },
    setActiveCardProgramSlug(state, val) {
      state.activeCardProgramSlug = val
    },
    setCountElementsOnPage(state, val) {
      state.countElementOnPage = val
    }
  },
  getters: {
    filteredProgramsList: (state) => {
      let result = []
      if (state.selectedProgramCategory === 0) {
        result = state.programsList
      } else {
        result = state.programsList.filter((value) => {
          return value.category.id === state.selectedProgramCategory
        })
      }
      if (state.searchString) {
        result = result.filter((value) => {
          return value.title.toLowerCase().includes(state.searchString.toLowerCase()) ||
            value.description.toLowerCase().includes(state.searchString.toLowerCase())
        })
      }
      return result
    },
    activeProgramBySlug: (state) => {
      let result = state.programsList.filter((value) => {
        return value.slug === state.activeCardProgramSlug
      })
      if (result.length > 0) {
        return result[0]
      }
      return []
    }
  },
  actions: {},
  modules: {
    cart
  }
})
